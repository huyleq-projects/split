#include "split.h"

#include "boost/graph/graphviz.hpp"

int main(int argc, char **argv){
    if(argc != 4){
        printf("Please specify # people, # transactions, # outputfilename\n");
        return 0;
    }
    
    int npeople = std::stoi(argv[1]);
    int ntransactions = std::stoi(argv[2]);
    std::string filename(argv[3]);
    gen_data(npeople, ntransactions, filename);
    return 0;
}
