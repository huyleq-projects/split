#include "split.h"

#include "boost/graph/graphviz.hpp"

int main(int argc, char **argv){
    if(argc != 2){
        printf("Please specify input filename\n ./bin/test.x filename\n");
        return 0;
    }
    
    std::string filename(argv[1]);
    std::vector<std::string> names;
    float *lend_matrix = process_input_data(names, filename);
    if(lend_matrix){
        process_lend_matrix(lend_matrix, names.size());
        print_lend_matrix(lend_matrix, names);

        int n = names.size();
        boost_digraph g;
        lend_matrix_to_dag(lend_matrix, n, g);
        int ntransfers = boost::num_edges(g);
        std::cout << "number of transfers: " << ntransfers << std::endl;
        
        std::ofstream f("debtdag.dot");
        boost_vertex_writer vertex_writer(g);
    	boost_edge_writer edge_writer(g);
        boost::write_graphviz(f, g, vertex_writer, edge_writer);

        std::vector<boost_vertex_descriptor> topo_order;
        dfs(g, &topo_order);
        std::cout << "topological order:";
        for(boost_vertex_descriptor u: topo_order) std::cout << " " << u;
        std::cout << std::endl;

        while(transfer_debts(g, topo_order));
        ntransfers = boost::num_edges(g);
        std::cout << "number of optimal transfers: " << ntransfers << std::endl;
        
        std::ofstream f1("optdag.dot");
        boost_vertex_writer vertex_writer1(g);
    	boost_edge_writer edge_writer1(g);
        boost::write_graphviz(f1, g, vertex_writer1, edge_writer1);
    }

    return 0;
}
