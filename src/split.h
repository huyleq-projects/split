#ifndef SPLIT_H
#define SPLIT_H

#include <vector>
#include <string>

#include "graph.h"

void gen_data(int npeople, int ntransactions, const std::string &filename);
float *process_input_data(std::vector<std::string> &names, const std::string &filename);
void print_lend_matrix(const float *lend_matrix, const std::vector<std::string> &names);
void process_lend_matrix(float *lend_matrix, int n);
void lend_matrix_to_dag(const float *lend_matrix, int n, boost_digraph &g);
bool transfer_debts(boost_digraph &g, const std::vector<boost_vertex_descriptor> &topo_order);

#endif
