#include <iostream>
#include <fstream>
#include <cstring>
#include <limits>
#include <unordered_map>
#include <utility>
#include <chrono>
#include <random>

#include "boost/algorithm/string.hpp"

#include "split.h"

void gen_data(int npeople, int ntransactions, const std::string &filename){
    std::ofstream outfile(filename);
    if(outfile.is_open()){
        std::vector<std::string> names(npeople);
        for(int i = 0; i < npeople; i++) names[i] = "P" + std::to_string(i);
        outfile << names[0];
        for(int i = 1; i < npeople; i++) outfile << " " << names[i];
        outfile << "\n";
        
        unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
        std::default_random_engine generator(seed);
        std::uniform_int_distribution<int> int_dist(0,npeople-1);
        std::uniform_real_distribution<float> float_dist(1, 1000);
        std::uniform_real_distribution<float> dist(0, 1);

        for(int i = 0; i < ntransactions; i++){
            int person = int_dist(generator), other = int_dist(generator);
            float amount = float_dist(generator);
            outfile << names[person] << " " << amount << " " << names[other];

            if(0 != other && dist(generator) > 0.5f) outfile << " " << names[0];
            for(int j = 1; j < npeople; j++){
                if(j != other && dist(generator) > 0.5f) outfile << " " << names[j];
            }
            outfile << "\n";
        }
    }
    else printf("CANNOT open file %s\n", filename.c_str());
    return;
}

float *process_input_data(std::vector<std::string> &names, const std::string &filename){
    std::ifstream infile(filename);
    if(infile.is_open()){
        std::string line; // first line contains everyone's names
        std::getline(infile, line);
        boost::split(names, line, [](char c){ return c == ' ';});
        
        int n = names.size(), nn = n*n;
        printf("number of people: %d\n", n);
        std::unordered_map<std::string, int> name_index_map;
        for(int i = 0; i < n; i++) name_index_map[names[i]] = i;
        
        // lending matrix in column major. each row is amount a person lends other
        float *lend_matrix = new float[nn];
        std::memset(lend_matrix, 0, nn*sizeof(float));
        
        // transactions are from second line on in format:
        // A x A B C, meaning A pays x dollars for A, B, and C
        std::string transaction;
        while(std::getline(infile, transaction)){
            
            std::vector<std::string> input;
            boost::split(input, transaction, [](char c){ return c == ' ';});
            
            if(input.size() < 3){
                printf("input transaction \"%s\" is NOT in valid format A x A B C\n", transaction.c_str());
                delete []lend_matrix;
                return nullptr;
            }
            
            int row = name_index_map[input[0]]; // who pays
            float amount = std::stof(input[1])/(input.size() - 2); // how much each other person owns
            for(int i = 2; i < input.size(); i++){
                int col = name_index_map[input[i]]; // the other person
                if(row != col) lend_matrix[row + col*n] += amount;
            }
        }

        return lend_matrix;
    }
    else{
        printf("CANNOT open file %s for input\n", filename.c_str());
        return nullptr;
    }
}

void print_lend_matrix(const float *lend_matrix, const std::vector<std::string> &names){
    int n = names.size();
    for(int col = 0; col < n; col++){ // each column is how much a person owns other people
        std::string name = names[col];
        printf("%s owns\n", name.c_str());
        for(int row = 0; row < n; row++){
            std::string other = names[row]; // the other person
            if(row != col) printf("\t%s %e\n", other.c_str(), lend_matrix[row + col*n]);
        }
    }
    return;
}

void process_lend_matrix(float *lend_matrix, int n){
    int n1 = n-1;
    for(int col = 0; col < n1; col++){
        for(int row = col+1; row < n; row++){
            int lower = row + col*n, upper = col + row*n;
            lend_matrix[lower] -= lend_matrix[upper];
            if(lend_matrix[lower] >= 0) lend_matrix[upper] = 0;
            else{
                lend_matrix[upper] = -lend_matrix[lower];
                lend_matrix[lower] = 0;
            }
        }
    }
    return;
}

void lend_matrix_to_dag(const float *lend_matrix, int n, boost_digraph &g){
    for(int i = 0; i < n; i++) boost::add_vertex(i, g); // add nodes for people
    for(unsigned col = 0; col < n; col++){ // add edges for debts
        for(unsigned row = 0; row < n; row++){
            float amount = lend_matrix[row + col*n];
            if(amount > 0) boost::add_edge(col, row, amount, g);
        }
    }

    std::vector<std::vector<int>> cycles; // find and remove cycles
    int ncycles = find_cycles(g, cycles);
    std::cout << "found: " << ncycles << " cycles" << std::endl;
    print_cycles(cycles);

    for(const std::vector<int> &cycle: cycles){
        int m = cycle.size(), m1 = m-1;
        bool edge_not_exist = false;
        float min_amount = std::numeric_limits<float>::max(), amount;

        for(int i = 0; i < m1; i++){ // find mininum weight edge
            boost_vertex_descriptor u = boost_vertex_descriptor(cycle[i]);
            boost_vertex_descriptor v = boost_vertex_descriptor(cycle[i+1]);
            std::pair<boost_edge_descriptor, bool> edge_pair = boost::edge(u, v, g);
            if(!edge_pair.second){ // this edge does not exist anymore
                edge_not_exist = true; break;
            }
            else if((amount = g[edge_pair.first].weight) < min_amount)
                min_amount = amount;
        }

        if(edge_not_exist) continue; // move on to next cycle
        else{ // substract minimum weight from every edge
            for(int i = 0; i < m1; i++){
                boost_vertex_descriptor u = boost_vertex_descriptor(cycle[i]);
                boost_vertex_descriptor v = boost_vertex_descriptor(cycle[i+1]);
                boost_edge_descriptor e = boost::edge(u, v, g).first;
                g[e].weight -= min_amount;
                if(g[e].weight == 0){
                    std::cout << "removing edge " << e << std::endl;
                    boost::remove_edge(e, g); // remove it
                }
            }
        }
    }
    return;
}

bool transfer_debt(boost_vertex_descriptor debtor,
                   boost_vertex_descriptor creditor,
                   boost_digraph &g,
                   const std::vector<boost_vertex_descriptor> &topo_order){
    std::vector<boost_vertex_descriptor> common_debtors; // who own both debtor and creditor
    float credit = 0; // to store how much common debtors own debtor
    int k = topo_order.size() - 1;
    boost_vertex_descriptor person;
    while((person = topo_order[k--]) != debtor){ // loop backward
        std::pair<boost_edge_descriptor, bool> e_debtor   = boost::edge(person, debtor,   g);
        std::pair<boost_edge_descriptor, bool> e_creditor = boost::edge(person, creditor, g);
        if(e_debtor.second && e_creditor.second){ // person owns both debtor and creditor
            credit += g[e_debtor.first].weight;
            common_debtors.push_back(person);
        }
    }

    std::pair<boost_edge_descriptor, bool> e = boost::edge(debtor, creditor, g);
    float debt = g[e.first].weight; // how much debtor owns creditor
    if(debt <= credit){ // debt can be transferred
        int ndebtors = common_debtors.size();
        for(int i = 0; i < ndebtors && debt > 0; i++){
            person = common_debtors[i];
            boost_edge_descriptor e_debtor   = boost::edge(person, debtor,   g).first;
            boost_edge_descriptor e_creditor = boost::edge(person, creditor, g).first;
            
            credit = std::min(debt, g[e_debtor].weight); // amount of debt to transfer
            g[e_debtor].weight   -= credit; // person owns debtor less
            g[e_creditor].weight += credit; // now person owns creditor more
            debt                 -= credit; // some debt has been transferred
            if(g[e_debtor].weight == 0) boost::remove_edge(e_debtor, g); // person does not own debtor anymore
        }
        boost::remove_edge(e.first, g); // all debt has been transferred
        return true;
    }
    else return false; // cant transfer debt
}

bool transfer_debts(boost_digraph &g, const std::vector<boost_vertex_descriptor> &topo_order){
    bool transferred = false;
    int n = boost::num_vertices(g);
    for(int i = 1; i < n; i++){
        boost_vertex_descriptor debtor = topo_order[i]; // debtor whose debt to be transferred
        for(int j = 0; j < i; j++){
            boost_vertex_descriptor creditor = topo_order[j]; // creditor who debtor owns
            if(boost::edge(debtor, creditor, g).second)
                transferred = (transfer_debt(debtor, creditor, g, topo_order)) || transferred;
        }

    }
    return transferred;
}
